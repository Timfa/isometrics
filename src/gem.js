/**
 * shiny
 */

var gem =
{
    gridPos: null,
    hvelocity: new vector(0, 0),
    vvelocity: 0,

    start: function()
    {
        this.renderer.subImage = Math.floor(Math.random() * this.renderer.image.subImages);

        this.transform.scale.x = 0;
        this.transform.scale.y = 0;

        var scaleUpTween = new TWEEN.Tween(this.transform.scale);

        scaleUpTween.to({x: [1.5, 1], y: [1.5, 1]}, 800);

        scaleUpTween.easing(TWEEN.Easing.Back.Out);

        scaleUpTween.start();

        this.shakeLength = 1000;
        this.shakes = 4;
        this.shakeAngle = 45;

        this.minShakeTimer = 5;
        this.maxShakeTimer = 20;
        this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));
        this.currentShakeTimer = 0;

        //GameAudio.play("bubble");
        this.ignoreCollisions = true;
    },

    update: function(deltaTime)
    {
        this.currentShakeTimer += deltaTime;

        if(this.currentShakeTimer >= this.shakeTimer)
        {
            this.shake();

            this.shakeTimer = this.minShakeTimer + (Math.random() * (this.maxShakeTimer - this.minShakeTimer));

            this.currentShakeTimer = 0;
        }

        let gridHeight = this.gridPos.grid.getHeight({x: Math.round(this.gridPos.x), y: Math.round(this.gridPos.y)}) + 0.6; //0.6: just over half a cell to nicely line them up with the top of the block

        this.gridPos.height += this.vvelocity * deltaTime;

        if(this.gridPos.height > gridHeight)
            this.vvelocity -= deltaTime * 10;
        else
        {
            if(this.vvelocity > -deltaTime)
            {
                this.vvelocity = 0;
                this.gridPos.height = gridHeight;
                this.hvelocity.x *= 0.9;
                this.hvelocity.y *= 0.9;
            }
            else
            {
                this.vvelocity *= -0.4

                this.hvelocity.x *= 0.99;
                this.hvelocity.y *= 0.99;
            }
        }


        if(this.gridPos.height < gridHeight - 0.1)
            this.gridPos.height = gridHeight - 0.1;

        if(this.gridPos.height <= 0.6)
            this.destroy();

        this.gridPos.x += this.hvelocity.x * deltaTime;
        this.gridPos.y += this.hvelocity.y * deltaTime;

        this.transform.position = this.gridPos.toScreen();
        this.renderer.layerDepth = this.gridPos.layerDepth + 0.7;
    },

    shake: function(callback, context)
    {
        var singleShakeTime = this.shakes / this.shakeLength;

        var shakeTween = new TWEEN.Tween(this.transform);

        var angles = [];

        var offset = Math.round(Math.random() * 5);

        for (var i = 0; i < this.shakes; i++)
        {
            var intensiy = 1 - (i / this.shakes);

            // Direction of the shake (is 'i' odd?)
            var dir = !((i + offset) % 2)? -1 : 1;

            var angleRad = this.shakeAngle;

            var angle = angleRad * dir * intensiy;

            angles.push(angle);
        }

        angles.push(0);

        shakeTween.to({rotation: angles}, this.shakeLength);

        shakeTween.easing(TWEEN.Easing.Quadratic.InOut);

        if (callback)
        {
            shakeTween.onComplete.addOnce(callback, context);
        }

        shakeTween.start();
    }
}