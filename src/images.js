/***
 * All images used in the game are  initialized here
 */

var s_gem = new image("images/gems.png", 72, 6);
var s_block = new image("images/block.png");
var s_stone = new image("images/stone.png");
var s_snowblock = new image("images/snowblock.png");
var s_sandblock = new image("images/sandblock.png");
var s_underblock = new image("images/underblock.png");
var s_water = new image("images/water.png");