@echo off

del ..\build\js\game.js

for %%I in (*.js) do (
    echo %%~fI
    type %%I >> ..\build\js\game.js
    echo. >> ..\build\js\game.js
    echo. >> ..\build\js\game.js
)

For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a:%%b)
echo %mydate% %mytime% >> ..\build\js\lastbuild.txt