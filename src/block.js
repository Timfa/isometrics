/**
 * tile
 */

var block =
{
    height: -2,
    TICK: 0,
    offset: 0,
    type: 0,
    maxHeight: 10,

    types:{
        SOLID: 0, LIQUID: 1
    },

    start: function()
    {
        this.renderer.visible = false;
        this.ignoreCollisions = true;
    },

    update: function(deltaTime)
    {
        var clickPos = GameInput.mousePosition;
			
        //hw = halfWidth, hh = halfHeight
        var hw = this.transform.width / 2;
        var hh = this.transform.height / 2;
        
        if(clickPos.x > this.transform.position.x - hw && clickPos.x < this.transform.position.x + hw)
        {
            if(clickPos.y > this.transform.position.y - hh && clickPos.y < this.transform.position.y + hh)
            {
                this.hover = true;
            }
        }

        this.renderer.visible = false;

        if(this.transform.position.x <= GameWindow.width + s_block.width
            && this.transform.position.x > -s_block.width
            && this.transform.position.y <= GameWindow.height + s_block.height
            && this.transform.position.y> -s_block.height)
            this.renderer.visible = true;

        if(this.renderer.visible)
        {
            if(this.type == block.types.LIQUID)
                this.offset = (Math.sin(this.transform.position.y * 0.01 + this.transform.position.x * 0.02 + block.TICK) + 1) * 5;
            else
                this.offset = 0;

            this.renderer.layerDepth = this.grid.transform.position.y + this.gridPos.x*2 + this.gridPos.y*2 + this.height
        }

        this.raisedThisFrame = false;

        if(this.height < -1 && this.filler)
        {
            this.destroy();
            this.grid.blocks = this.grid.blocks.filter(a => !a.destroyed && a != this)
        }
        
        this.height = Math.max(-1, Math.min(this.height, block.maxHeight))
    },

    draw: function()
    {
        if(!this.filler)
        {
            if(this.height > 7)
            {
                this.renderer.image = s_snowblock;
                this.type = block.types.SOLID
            }
            else if(this.height > 0.7)
            {
                this.renderer.image = s_block;
                this.type = block.types.SOLID
            }
            else
            {
                this.renderer.image = s_sandblock;
                this.type = block.types.SOLID
            }
            
            if(this.height <= 0)
            {
                this.renderer.image = s_water;
                this.type = block.types.LIQUID
            }
        }
        
        this.defaultDraw();
    }
}