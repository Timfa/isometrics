/**
 * tiles
 */

 let a = 0;
 let b = 0;
 let c = 0;
 let d = 0;

 let det = 0;

 let ai = 0;
 let bi = 0;
 let ci = 0;
 let di = 0;

 function lerp(a, b, t)
 {
    return a + (b-a) * t;
 }

var kernel = 
[
    new vector(-1, -1), new vector(-1, 0), new vector(-1, 1),
    new vector(0, -1), new vector(0, 1),
    new vector(1, -1), new vector(1, 0), new vector(1, 1)
]

var grid =
{
    blocks: [],
    width: 25,
    height: 25,
    tick: 0,
    canRaise: false,
    mousePos: new vector(),

    start: function()
    {
        a = 0.5 * s_block.width;
        b = -0.5 * s_block.width;
        c = 0.25 * s_block.height;
        d = 0.25 * s_block.height;
       
        det = (1 / (a * d - b * c));
       
        ai = det * d;
        bi = det * -b;
        ci = det * -c;
        di = det * a;

        console.log("Spawning");

        for(var x = 0; x < this.width; x++)
        {
            for(var y = 0; y < this.height; y++)
            {
                var pos = this.gridToScreen(new vector(x, y));
                var blocki = GameObject.create(block, pos.x, pos.y, s_block);

                blocki.height = 0;

                blocki.gridPos = new vector(x, y);
                blocki.grid = this;
                this.blocks.push(blocki);
            }
        }
    },

    addBlock: function(gridPos, height = -999, img = s_block, filler = false)
    {
        let nblock = GameObject.create(block, -100, -100, img)
        nblock.height = height > -999? height : this.getHeight(gridPos) + 1;
        nblock.gridPos = gridPos;
        nblock.transform.position = this.gridToScreen(nblock.gridPos, nblock.height)
        nblock.grid = this;
        nblock.filler = filler
        this.blocks.push(nblock)
    },

    getStackHeight: function(gridPos)
    {
        return this.getBlocksAt(gridPos).length;
    },

    getHeight: function(gridPos)
    {
        let height = -1000;
        for(var i = 0; i < this.blocks.length; i++)
        {
            if(this.blocks[i].gridPos.x == gridPos.x && this.blocks[i].gridPos.y == gridPos.y && height < this.blocks[i].height)
                height = this.blocks[i].height;
        }

        return height;
    },

    getDepth: function(gridPos)
    {
        let height = 1000;
        for(var i = 0; i < this.blocks.length; i++)
        {
            if(this.blocks[i].gridPos.x == gridPos.x && this.blocks[i].gridPos.y == gridPos.y && height > this.blocks[i].height)
                height = this.blocks[i].height;
        }

        return height;
    },

    raiseStackAt: function(gridPos, alt, mult = 1.21)
    {
        if(this.getHeight(gridPos) >= block.maxHeight)
            return;

        let blocksToRaise = this.getBlocksAt(gridPos);
        
        if(blocksToRaise[0].raisedThisFrame)
            return;

        for(let i = 0; i < blocksToRaise.length; i++)
        {
            blocksToRaise[i].height += alt;
            blocksToRaise[i].raisedThisFrame = true;
        }
        
        let depth = this.getDepth(gridPos);

        if(depth >= 0.8 && (this.getHeight(gridPos.add(new vector(1, 0))) < (depth-1) || this.getHeight(gridPos.add(new vector(0, 1))) < depth-1))
        {
            let stone = blocksToRaise.length + Math.round(Math.random() * Math.random()) > 2
            let block = this.addBlock(gridPos, depth - 1, stone? s_stone : s_underblock, true)
        }

        for(let i = 0; i < kernel.length; i++)
        {
            let height = this.getHeight(gridPos.add(kernel[i]));

            let m = Math.abs(kernel[i].x) + Math.abs(kernel[i].y)
            let diff = height - (this.getHeight(gridPos) - Math.ceil(m) * (mult - 1))

            if(height > -500 && diff<0)
            {
                this.raiseStackAt(gridPos.add(kernel[i]), -diff * 0.2, Math.max(0, mult * mult));
            }
        }
    },

    lowerStackAt: function(gridPos, alt)
    {
        let blocksToRaise = this.getBlocksAt(gridPos);
        for(let i = 0; i < blocksToRaise.length; i++)
            blocksToRaise[i].height -= alt;
        
        let depth = this.getDepth(gridPos);

        for(let i = 0; i < kernel.length; i++)
        {
            let height = this.getHeight(gridPos.add(kernel[i]));
            if(height > -500 && height > this.getHeight(gridPos) + Math.ceil(kernel[i].length())*0.7)
            {
                this.lowerStackAt(gridPos.add(kernel[i]), alt);
            }
        }
    },

    getBlocksAt: function(gridPos)
    {
        let list = [];
        for(var i = 0; i < this.blocks.length; i++)
        {
            if(this.blocks[i].gridPos.x == gridPos.x && this.blocks[i].gridPos.y == gridPos.y)
            {
                list.push(this.blocks[i]);
            }
        }

        return list;
    },

    update: function(deltaTime)
    {
        this.transform.position.x = GameWindow.width / 2

        block.TICK += deltaTime;

        let selectedBlock = null;

        for(var i = 0; i < this.blocks.length; i++)
        {
            if(this.blocks[i].hover && (selectedBlock == null || selectedBlock.renderer.layerDepth < this.blocks[i].renderer.layerDepth))
                selectedBlock = this.blocks[i];

            let offset = 0;
            if(this.blocks[i].gridPos.x == this.mousePos.x && this.blocks[i].gridPos.y == this.mousePos.y)
                offset = 7 + Math.sin(block.TICK*7);

            let fromy = this.blocks[i].transform.position.y;
            this.blocks[i].transform.position = this.gridToScreen(this.blocks[i].gridPos, this.blocks[i].type == block.types.SOLID? this.blocks[i].height : 0)
            let toy = this.blocks[i].transform.position.y - offset + this.blocks[i].offset;
            this.blocks[i].transform.position.y = lerp(fromy, toy, 10 * deltaTime);

            this.blocks[i].hover = false;
        }

        if(GameInput.g_middleMouseDown || GameInput.isHeld(32))
        {
            console.log("gem")
            let screenPos = this.gridToScreen(this.mousePos, 50)
            let ngem = GameObject.create(gem, screenPos.x, screenPos.y, s_gem);
            ngem.gridPos = new gridvector(this, this.mousePos.x, this.mousePos.y, this.getHeight(this.mousePos) + 5);

            ngem.hvelocity = new vector((-0.5 + Math.random()) * 3, (-0.5 + Math.random()) * 3)
        }

        if(selectedBlock != null)
        {
            this.mousePos = selectedBlock.gridPos;

            if(GameInput.g_mouseDown && this.canRaise)
            {
                this.raiseStackAt(selectedBlock.gridPos, deltaTime * 3)
            }

            if(GameInput.g_altMouseDown && this.canRaise)
            {
                this.lowerStackAt(selectedBlock.gridPos, deltaTime * 3)
            }
        }
    },

    screenToGrid: function(screenPosition)
    {
        screenPosition = screenPosition.subtract(this.transform.position);
        screenPosition.y += s_block.height / 4;

        return new vector(
            (screenPosition.x * ai + screenPosition.y * bi),
            (screenPosition.x * ci + screenPosition.y * di)
            )
    },

    gridToScreen: function(gridPosition, altitude = 0)
    {
        return new vector(
            this.transform.position.x + (gridPosition.x * 0.5 * s_block.width + gridPosition.y * -0.5 * s_block.width),
            this.transform.position.y + ((gridPosition.x * 0.25 * s_block.height + gridPosition.y * 0.25 * s_block.height) - (altitude * s_block.height * 0.5))
            )
    }
}