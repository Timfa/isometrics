/**
 * Main script
 */

window.onload = init;

function DoInheritance()
{
    
}

function init()
{
    DoInheritance();

    var width = window.innerWidth - 2;
    var height = window.innerHeight - 2;

    Game.initialize(0, 0, width, height, 600, gameStart);
}

function gameStart()
{
	GameWindow.backgroundColor = {r:100, g:149, b:237};

	Game.setUpdateFunction(update); // Let the game engine know we want to link our 'update()' to the game's update step.

	GameAudio.init(sounds, soundsFolder);

    var grid1 = GameObject.create(grid, 650, 50);
    grid1.canRaise = true;
}

function update(deltaTime)
{  
    GameWindow.width = window.innerWidth - 2;
    GameWindow.height = window.innerHeight - 2;
}