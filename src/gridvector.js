/**
 * easy grid positions
 */

class gridvector
{
    x = 0;
    y = 0;
    height = 0;
    grid = null;

    constructor(grid, x = 0, y = 0, h = 0)
    {
        this.x = x;
        this.y = y;
        this.height = h;
        this.grid = grid;
    }

    get layerDepth()
    {
        return this.grid.transform.position.y + this.x*2 + this.y*2 + this.height;
    }

    toScreen()
    {
        return this.grid.gridToScreen(this, this.height);
    }
}